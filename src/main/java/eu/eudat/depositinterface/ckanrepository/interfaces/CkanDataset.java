package eu.eudat.depositinterface.ckanrepository.interfaces;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CkanDataset {

    private String name;
    @JsonProperty("private")
    private boolean isPrivate;
    private String author;
    private String author_email;
    private String notes;
    private String version;
    private String owner_org;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public boolean isPrivate() {
        return isPrivate;
    }
    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public String getAuthor() {
        return author;
    }
    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAuthor_email() {
        return author_email;
    }
    public void setAuthor_email(String author_email) {
        this.author_email = author_email;
    }

    public String getNotes() {
        return notes;
    }
    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getVersion() {
        return version;
    }
    public void setVersion(String version) {
        this.version = version;
    }

    public String getOwner_org() {
        return owner_org;
    }
    public void setOwner_org(String owner_org) {
        this.owner_org = owner_org;
    }

}
