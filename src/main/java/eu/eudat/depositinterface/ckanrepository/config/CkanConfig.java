package eu.eudat.depositinterface.ckanrepository.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.eudat.depositinterface.repository.RepositoryDepositConfiguration;

public class CkanConfig {
    private enum DepositType {
        SystemDeposit(0), UserDeposit(1), BothWaysDeposit(2);

        private final int value;

        DepositType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static DepositType fromInteger(int value) {
            switch (value) {
                case 0:
                    return SystemDeposit;
                case 1:
                    return UserDeposit;
                case 2:
                    return BothWaysDeposit;
                default:
                    throw new RuntimeException("Unsupported Deposit Type");
            }
        }
    }

    @JsonProperty("depositType")
    private int depositType;
    @JsonProperty("repositoryId")
    private String repositoryId;
    @JsonProperty("apiToken")
    private String apiToken;
    @JsonProperty("repositoryUrl")
    private String repositoryUrl;
    @JsonProperty("repositoryRecordUrl")
    private String repositoryRecordUrl;
    @JsonProperty("organization")
    private String organization;
    @JsonProperty("hasLogo")
    private boolean hasLogo;
    @JsonProperty("logo")
    private String logo;

    public int getDepositType() {
        return depositType;
    }
    public void setDepositType(int depositType) {
        this.depositType = depositType;
    }

    public String getRepositoryId() {
        return repositoryId;
    }
    public void setRepositoryId(String repositoryId) {
        this.repositoryId = repositoryId;
    }

    public String getApiToken() {
        return apiToken;
    }
    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public String getRepositoryUrl() {
        return repositoryUrl;
    }
    public void setRepositoryUrl(String repositoryUrl) {
        this.repositoryUrl = repositoryUrl;
    }

    public String getRepositoryRecordUrl() {
        return repositoryRecordUrl;
    }
    public void setRepositoryRecordUrl(String repositoryRecordUrl) {
        this.repositoryRecordUrl = repositoryRecordUrl;
    }

    public String getOrganization() {
        return organization;
    }
    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public boolean isHasLogo() {
        return hasLogo;
    }
    public void setHasLogo(boolean hasLogo) {
        this.hasLogo = hasLogo;
    }

    public String getLogo() {
        return logo;
    }
    public void setLogo(String logo) {
        this.logo = logo;
    }

    public RepositoryDepositConfiguration toRepoConfig() {
        RepositoryDepositConfiguration config = new RepositoryDepositConfiguration();
        config.setDepositType(this.depositType);
        config.setRepositoryId(this.repositoryId);
        config.setRepositoryUrl(this.repositoryUrl);
        config.setRepositoryRecordUrl(this.repositoryRecordUrl);
        config.setHasLogo(this.hasLogo);
        return config;
    }
}
