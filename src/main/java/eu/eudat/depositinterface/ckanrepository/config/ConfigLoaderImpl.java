package eu.eudat.depositinterface.ckanrepository.config;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Service("ckanConfigLoader")
public class ConfigLoaderImpl implements ConfigLoader{
    private static final Logger logger = LoggerFactory.getLogger(ConfigLoaderImpl.class);
    private static final ObjectMapper mapper = new ObjectMapper();

    private List<CkanConfig> ckanConfigs = new ArrayList<>();

    private final Environment environment;

    @Autowired
    public ConfigLoaderImpl(Environment environment){
        this.environment = environment;
    }

    @Override
    public List<CkanConfig> getCkanConfig() {
        if(ckanConfigs == null || ckanConfigs.isEmpty()) {
            try{
                ckanConfigs = mapper.readValue(getStreamFromPath(environment.getProperty("configuration.ckan")), new TypeReference<List<CkanConfig>>() {});
            } catch (IOException e) {
                logger.error(e.getLocalizedMessage(), e);
            }
        }
        return ckanConfigs;
    }

    @Override
    public byte[] getLogo(String repositoryId) {
        if (!ckanConfigs.isEmpty()) {
            CkanConfig ckanConfig = ckanConfigs.stream().filter(x -> x.getRepositoryId().equals(repositoryId)).findFirst().orElse(null);
            if (ckanConfig != null) {
                String logo = ckanConfig.getLogo();
                InputStream logoStream;
                if (logo != null && !logo.isEmpty()) {
                    logoStream = getStreamFromPath(logo);
                }
                else {
                    logoStream = getClass().getClassLoader().getResourceAsStream("ckan.png");
                }
                try {
                    return (logoStream != null) ? logoStream.readAllBytes() : null;
                }
                catch (IOException e) {
                    logger.error(e.getMessage(), e);
                }
            }
            return null;
        }
        return null;
    }

    private InputStream getStreamFromPath(String filePath) {
        try {
            return new FileInputStream(filePath);
        } catch (FileNotFoundException e) {
            logger.info("loading from classpath");
            return getClass().getClassLoader().getResourceAsStream(filePath);
        }
    }
}
