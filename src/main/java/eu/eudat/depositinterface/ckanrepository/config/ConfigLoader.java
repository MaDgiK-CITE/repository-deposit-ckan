package eu.eudat.depositinterface.ckanrepository.config;

import java.util.List;

public interface ConfigLoader {
    byte[] getLogo(String repositoryId);
    List<CkanConfig> getCkanConfig();
}
